#!/bin/bash
#Скрипт по пакетной установки программ с возможностью их выбора 
#Автор: Александр Клич, сайт https://prostolinux.my1.ru
#"Спасибо klichalex автору скрипта Transformation! https://www.youtube.com/user/klichalex/featured"
#"Спасибо klichalex автору хороших видео уроков! https://rutube.ru/channel/23628980/videos/"
#"Спасибо klichalex автору перевода Refracta на Русский язык http://prostolinux.my1.ru/"
#Дополнение chebur chebur3133@gmail.com https://prostolinux.my1.ru/index/st/0-11 https://gitlab.com/Chebur70 https://try.gitea.io/Chebur70"
#"Спасибо circulosmeos https://github.com/circulosmeos за предоставленые скрипты"
#"Спасибо разработчику скриптов Dmitriy wj42ftns Chekhov, Russia https://gist.github.com/wj42ftns"
#"Спасибо kachnu https://github.com/kachnu за предоставленые скрипты"
#"Спасибо Systemback https://gitlab.com/Kendek/systemback"
#"Спасибо LeCorbeau's Vault https://lecorbeausvault.wordpress.com/2021/01/10/quickly-build-a-custom-bootable-installable-debian-live-iso-with-live-build/"
#"Спасибо Франко Кониди https://syslinuxos.com/things-to-do-after-installing-syslinuxos-12/"
#"Нет недостижимых целей,есть высокий коэффициент лени,недостаток смекалкии и запас отговорок"
#"Не важно насколько медленно ты движешься, главное не останавливаться.Конфуций"
#"Наш ответ Чемберлену!Дави Империализма Гиену Могучий Рабочий Класс! Вчера были танки лишь у Чемберлена,А нынче есть и у нас!"
#"Запомните,чтобы ничего не делать, надо уметь делать все" 
#"Я не потерпел неудачу, я нашел 10 000 способов, которые не сработают-Эдисон."

echo "Введите имя Вашей учетной записи и нажмите Enter"
read name
echo "Имя - $name"

cd ~

#Добавление архитектуры i386
sudo dpkg --add-architecture i386 
sudo apt update && sudo apt full-upgrade -y
#Установка обязательных программ
sudo apt install -y sudo curl wget apt-transport-https gdebi dirmngr aptitude engrampa onboard unzip libfuse2 
#Удаление старых настроек 
rm -Rf ~/.config/xfce4/
#Удоление папки autostart
rm -Rf ~/.config/autostart/
#Удоление  
sudo rm -rf /etc/xdg/autostart/sparky-locker.desktop
sudo rm -rf /etc/xdg/autostart/polkit-gnome-authentication-agent-1.desktop
#Добавление папки autostart
mkdir ~/.config/autostart 
#Добавление папки applications
mkdir ~/.local/share/applications 
#Добавление папки Uninstall
mkdir ~/'Desktop'/Uninstall
#Настройка теминала
rm -rf .bashrc
wget https://gitlab.com/Chebur70/Setting/-/raw/main/bashrc.tar.gz
tar xvf bashrc.tar.gz && rm -rf bashrc.tar.gz
#Appearance
wget https://gitlab.com/Chebur70/Setting/-/raw/main/appearance.tar.gz 
tar xvf appearance.tar.gz && rm -rf appearance.tar.gz
chmod -R 777 appearance
#Applications
wget https://gitlab.com/Chebur70/Setting/-/raw/main/applications.tar.gz 
tar xvf applications.tar.gz && rm -rf applications.tar.gz
chmod -R 777 applications
cp -R applications ~/.local/share/ && rm -Rf applications
#Icons
wget https://gitlab.com/Chebur70/Setting/-/raw/main/icons.tar.gz
tar xvf icons.tar.gz && rm -rf icons.tar.gz
chmod -R 777 icons
sudo cp -R icons /usr/share/ && rm -Rf icons
#Themes
wget https://gitlab.com/Chebur70/Setting/-/raw/main/themes.tar.gz
tar xvf themes.tar.gz && rm -rf themes.tar.gz
chmod -R 777 themes
sudo cp -R themes /usr/share/ && rm -Rf themes
#Backgrounds
wget https://gitlab.com/Chebur70/Setting/-/raw/main/backgrounds.tar.gz
tar xvf backgrounds.tar.gz && rm -rf backgrounds.tar.gz
chmod -R 777 backgrounds
sudo cp backgrounds/05.jpg /boot/grub
sudo update-grub
sudo cp -R backgrounds /usr/share/ && rm -Rf backgrounds
#Install gdebi от Klichalex
wget https://gitlab.com/Chebur70/Setting/-/raw/main/gdebi.desktop.tar.gz
tar xvf gdebi.desktop.tar.gz && rm -rf gdebi.desktop.tar.gz
sudo mv gdebi.desktop /usr/share/applications/
# Установка программ по выбору.
sudo apt install -y dialog
echo "================================="
echo "ВАШ БЕСПРОВОДНОЙ КОНТРОЛЕР,запомните его."
echo "(Если строка ниже пустая, то его нет)"
lspci | grep -i Network
echo "================================="
sleep 13s

cmd=(dialog --separate-output --checklist "Выберите программное обеспечение, которое вы хотите установить:" 22 76 16)
	options=(1 "Grub themes Rosa" on  #любой параметр может быть установлен по умолчанию на "off"
		  2 "Plymout themes Percentage" off
		  3 "QtFsarchiver-утилита для клонирования диска под Linux" off
		  4 "BackUpRestore-7.0-системы Linux" off
		  5 "Systemback-Резервное копирование" off
		  6 "Slimjet-быстрый и безопасный веб-браузер" off
		  7 "PaleMoon-браузер" off
		  8 "GoogleChrome-stable-браузер" off
		  9 "Chromium-браузер" off
		10 "Opera-stable-браузер" off
		11 "Vivaldi-браузер" off
		12 "Minbrowser-браузер" off
		13 "Brave-браузер с открытым исходным кодом" off
		14 "Torbrowser-мощный инструмент для защиты приватности и онлайновых свобод" off
		15 "LibreWolf-Браузер, фокусирующийся на безопасности" off
		16 "Gvidm-утилита для быстрого изменения разрешения экрана" off
		17 "Qshutdown-утилита для отключения компьютера" on
		18 "Bucklespring-консольная утилита для эмуляции "щелчков" механической клавиатуры " on
		19 "ThunarOfRoot-Работа с архивами в xfce4" on
		20 "CompizEmerald-композитный менеджер" off
		21 "Stacer-Оптимизация, очистка, настройка системы" off
		22 "BleachBit-инструмент,освободит диск ,обеспечив безопасность данных" off
		23 "GKrellM-виджет системных мониторов-Linux" off
		24 "RamboxAppimage-управляйте всеми своими коммуникационными аккаунтами из этого приложения" off
		25 "FranzAppimage-приложение для обмена gdown.pl-1.4сообщениями" off
		26 "SkypeWeb-мессенджер" off
		27 "ZoomAppimage-Коференция" off
		28 "TelegramWeb-мессенджер" off
		29 "Signal-мессенджер" off
		30 "WireWeb-приложение для зашифрованной связи" off
		31 "WhatsAppWeb-мессенджер" off
		32 "ViberAppimage-мессенджер" off
		33 "SMSMessagesWeb-сообщения на компьютере" off
		34 "GoogleTranslateWeb-позволяет мгновенно переводить слова" on
		35 "SpeedtestNetWeb-скорость интернета и задержку соединения." off
		36 "AppImageUpdate-обновляет на основе информации встроенной в AppImage" off
		37 "BauhAppimage-магазин приложений AppImage, AUR, Flatpaks и Snaps для Linux" off
		38 "Anydesk-удаленная помощь" off
		39 "Detect It Easy-Определяет тип файла в Linux" off
		40 "FreeTubeAppimage-Частный клиент YouTube" off
		41 "Cherrytree-Приложение для создания иерархических заметок" on
		42 "MxBootRepairAppimage-Восстановление загрузки" off
		43 "MXBootOptionsAppimage-Параметры загрузки" off
		44 "MXMenuEditorAppimage-Меню-редактор" off
		45 "ddCopy-создания загрузочных Live USB, копирования ISO-образов" off
		46 "UNetbootin-Утилита для создания Live USB" off
		47 "EtcherAppimage-предназначенное для записи файлов образов дисков" off
		48 "RosaImageWriter-Запись ISO-образов на USB-диск" off
		49 "DDRescue-GUI-инструмент для восстановления данных" off
		50 "MultiBootUSB-создания мультизагрузочных USB-носителей" off
		51 "Ventoy-создание мультизагрузочной флешки" off
		52 "Mintstick-Графический интерфейс для записи файлов .img или .iso на USB" off
		53 "CapacityTester-Проверка USB-накопителя или карты памяти,является ли это поддельной" off
		54 "MKUSB-создания постоянного хранения данных Live USB" off
		55 "LiveUSBMakerQtAppImage-создания загрузочных Live USB, копирования ISO-образов" off
		56 "LiveUSBMakerAppImage-создания загрузочных Live USB, копирования ISO-образов" off
		57 "DDLiveUsb.AppImage-создания загрузочных Live USB, копирования ISO-образов" off
		58 "DDLiveUSBSh+LiveUSBMakerSh-создания загрузочных Live USB, копирования ISO-образов" off
		59 "Любая Ваша программа" off
		60 "Дополнительные драйвера Wi-Fi Broadcom" off
		61 "Дополнительные драйвера Wi-Fi Atheros" off
		62 "Дополнительные драйвера Wi-Fi Realtek" off
		63 "Дополнительные драйвера Wi-Fi Intel" off
		64 "Дополнительные драйвера Wi-Fi Marvell и NXP (Libertas" off
		65 "Любая Ваша программа" off
		66 "Любая Ваша программа" off
		67 "Любая Ваша программа" off
		68 "Cutter-это бесплатная платформа обратного проектирования" off
		69 "Gotop-монитор графической активности на основе терминалов" off
		70 "GImageReader-извлечение текста из изображений и PDF-файлов в Linux" off
		71 "Любая Ваша программа" off
                72 "MOCP-Терминальный музыкальный плеер" off 
                73 "Transmission-простой BitTorrent-клиент c открытым исходным кодом" off
                74 "qBittorrent-бесплатный клиент для файлообменной сети BitTorrent" off
                75 "Persepolis-менеджер загрузок и графический интерфейс для Aria2" off
                76 "ClipGrabAppimage-это бесплатный загрузчик и конвертор видео из YouTube" off
                77 "XtremeDownloadManager-зaгрузчик файлов" off
                78 "YoutubeDL+Gui-Консольный+Графический интерфейс медиа-загрузчика" off
                79 "Yt-dlp-это очень хороший и мощный форк известной утилиты Youtube-dl" off
                80 "JDownloader-это бесплатный инструмент управления загрузками" off
                81 "VideomassApp-это кроссплатформенный графический интерфейс для FFMPEG и YouTube-DL / YT-DLP" off
                82 "MediaDownloader-Загружает видео" off
                83 "MotrixAppimage-загружайте большие файлы без сбоев" off
                84 "Любая Ваша программа" off
                85 "Geany-среда разработки программного обеспечения, написанная с использованием библиотеки GTK+" off
                86 "FeatherPad-бесплатный текстовый редактор" off
                87 "Pluma-легковерный текстовый редактор" off
                88 "Gedit-свободный текстовый редактор" off
                89 "Medit-это более быстрый аналог Gedit" on
                90 "Leafpad-текстовый редактор" off
                91 "LibreOffice-офисный пакет с открытым исходным кодом" off
                92 "OpenOffice-свободный пакет офисных приложений, основан на коде StarOffice" off
                93 "Freeoffice-Бесплатный полнофункциональный офисный пакет" off
                94 "OnlyOffice-офисный пакет с открытым исходным кодом" off
                95 "WPSOffice-офисный пакет" off
                96 "Conky-мощный и легко настраиваемый системный монитор" off
                97 "GnomePie-лаунчер для быстрого запуска других программ" off
                98 "Plank-простая док-панель" off
                99 "Любая Ваша программа" off
              100 "Речевой Сервер Speech Dispatcher" off
              101 "Из каталога deb-Любые debпакеты с зависимостями что закинете" off)

		choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
		clear
		for choice in $choices
		do
		    case $choice in
	        	      1)
	            		#Install GrubThemesRosa
				echo "================ Установка GrubThemesRosa ================"								 
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/Rosa.tar.gz
                                tar xvf Rosa.tar.gz && rm -rf Rosa.tar.gz   
                                sudo mkdir /boot/grub/themes/     
				mv rosa/GrubThemesRosaUninstall.sh ~/'Desktop'/Uninstall                                                   
                                sudo mv rosa /boot/grub/themes/rosa                                                                                                                                                                                                                                                          
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GrubRosa.tar.gz
                                tar xvf GrubRosa.tar.gz && rm -rf GrubRosa.tar.gz                                    
                                sudo mv grub /etc/default/grub                                                          
                                sudo update-grub                        
				;;
			     2)
				#Install PlymoutThemesPercentage
				echo "================ Установка PlymoutThemesPercentage ================"  
                                sudo apt install -y plymouth-themes                                                                                                                                                                                                                                                                                                                                                    
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GrubRosa.tar.gz
                                tar xvf GrubRosa.tar.gz && rm -rf GrubRosa.tar.gz     
                                sudo mv grub /etc/default/grub 
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/Percentage.tar.gz
                                tar xvf Percentage.tar.gz && rm -rf Percentage.tar.gz 
				mv percentage/PlymoutThemesPercentageUninstall.sh ~/'Desktop'/Uninstall                                                              
                                sudo mv percentage /usr/share/plymouth/themes/percentage                                                                                        
                                sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth \
				/usr/share/plymouth/themes/percentage/percentage.plymouth 01
                                sudo plymouth-set-default-theme -R percentage
                                sudo update-initramfs -u -k all
                                sudo update-grub                                                                                                                      
				;;
			     3)
                                #Install QtFsarchiverApt
				echo "================ Установка QtFsarchiverApt ================"
				sudo apt install -y qt-fsarchiver
                                wget https://www.dropbox.com/sh/bi9txuzgd6r3ehr/AAAAlNIg-rGhJgC38L3yEBjza?dl=0 && \
				unzip -q 'AAAAlNIg-rGhJgC38L3yEBjza?dl=0'                                                                                            
				chmod +x QtFsarchiver/qt-fsarchiver.desktop                                                         
                                cp QtFsarchiver/qt-fsarchiver.desktop ~/.local/share/applications/
                                sudo cp QtFsarchiver/QtFsarchiver.svg /usr/share/icons/                                                                    				
				cp QtFsarchiver/Qt5Fsarchiver.png ~/'Desktop'/	
				cp QtFsarchiver/QtFsarchiverUninstall.sh ~/'Desktop'/Uninstall	
                                rm -rf 'AAAAlNIg-rGhJgC38L3yEBjza?dl=0' && rm -Rf QtFsarchiver
				;;
			     4)
				#Install BackUpRestoreSh-7.0
			        echo "================ Установка BackUpRestoreSh-7.0 ================"
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/libglade2-0_2.6.4-2.4_amd64.deb
				wget https://gitlab.com/Chebur70/Setting/-/raw/main/gtkdialog_0.8.3-2mx23+1_amd64.deb
                                sudo dpkg -i *.deb
                                sudo apt install -fy
				sudo apt autoremove -y
                                rm -rf gtkdialog* libglade2*
                                sudo apt install -y git tar rsync wget gdisk openssl gzip bzip2 xz-utils pigz pbzip2 pixz
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/BackUpRestoreSh.tar.gz 
                                tar xvf BackUpRestoreSh.tar.gz && rm -rf BackUpRestoreSh.tar.gz
				chmod +x BackUpRestore/*.desktop				
                                cp BackUpRestore/BackUpRestore.desktop ~/.local/share/applications/
				cp BackUpRestore/BackUpRestoreTerminal.desktop ~/.local/share/applications/
				chmod -R 777 BackUpRestore/BackUp
                                sudo cp -R BackUpRestore/BackUp /usr/share/icons/
				chmod +x BackUpRestore/BackUpRestore/*.sh
				sudo cp -R BackUpRestore/BackUpRestore /usr/local/bin/
				cp BackUpRestore/BackUp.png ~/'Desktop'/
				cp BackUpRestore/Restore.png ~/'Desktop'/
				cp BackUpRestore/BackUpRestoreUninstall.sh ~/'Desktop'/Uninstall
			        rm -Rf BackUpRestore   
				;;
			     5)
				#Install Systemback
				echo "================ Установка Systemback ================"				 
				sudo apt-get install -y systemback
				wget https://www.dropbox.com/sh/lea61fkqh0xd52g/AABqAEwpUr26hE1-DDgQpUYba?dl=0 && \
				unzip -q 'AABqAEwpUr26hE1-DDgQpUYba?dl=0'  
				sudo rm -rf /usr/share/systemback/lang/systemback_ru.qm
                                sudo cp Systemback/systemback_ru.qm  /usr/share/systemback/lang
				cp Systemback/SystembackUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AABqAEwpUr26hE1-DDgQpUYba?dl=0' && rm -Rf Systemback
				;;
			     6)
				#Install Slimjet
			        echo "================ Установка Slimjet ================"
				sudo apt install -y slimjet
                                wget https://www.dropbox.com/sh/cvwwvvvsdjh98jh/AACjiNduWLJJbgqqcHVSqPNpa?dl=0 && \
				unzip -q 'AACjiNduWLJJbgqqcHVSqPNpa?dl=0'                
				tar xvf Slimjet/slimjet.tar.gz    
                                mv slimjet ~/.config/slimjet
				cp Slimjet/SlimjetUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AACjiNduWLJJbgqqcHVSqPNpa?dl=0' && rm -Rf Slimjet                                                                                                 
				;;
			     7)
				#Install PaleMoon
				echo "================ Установка PaleMoon ================"
				sudo apt install -y palemoon
                                wget https://www.dropbox.com/sh/had6irovtbdciqq/AAAM-XymjB8FxiHQT-ILBfcZa?dl=0 && \
				unzip -q 'AAAM-XymjB8FxiHQT-ILBfcZa?dl=0'	
				chmod +x PaleMoon/palemoon.desktop
                                cp PaleMoon/palemoon.desktop ~/.local/share/applications/
                                sudo cp PaleMoon/Palemoon.png /usr/share/icons/			 
				tar xvf PaleMoon/MoonchildProductions.tar.gz
                                chmod u+x '.moonchild productions'
				cp PaleMoon/PaleMoonUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAM-XymjB8FxiHQT-ILBfcZa?dl=0' && rm -Rf PaleMoon	 
				;;
			     8) 
				#Install GoogleChromeStable 
		                echo "================ Установка GoogleChromeStable ================"
				wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
                                sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" \
				>> /etc/apt/sources.list.d/google.list'
                                sudo apt update && sudo apt install -y google-chrome-stable  		 
				wget https://www.dropbox.com/sh/0wi9h2a7ej64xz7/AAAQNJGMkPEt1huEQJ9esjjPa?dl=0 && \
				unzip -q 'AAAQNJGMkPEt1huEQJ9esjjPa?dl=0'      				 
				chmod +x GoogleChrome/google-chrome.desktop                                                                                                                
                                cp GoogleChrome/google-chrome.desktop ~/.local/share/applications/
                                sudo cp GoogleChrome/GoogleChrome.png /usr/share/icons/
                                tar xvf GoogleChrome/google-chrome.tar.gz 
                                mv google-chrome ~/.config/google-chrome
				cp GoogleChrome/GoogleChromeStableUninstall.sh ~/'Desktop'/Uninstall   
                                rm -rf 'AAAQNJGMkPEt1huEQJ9esjjPa?dl=0' && rm -Rf GoogleChrome                                 
                               # echo 'Очистка дублей репозитериев (Chrome - создаёт дубли)'  
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/aptsources-cleanup.pyz
                                printf 'yes\n' | sudo python3 -OEs aptsources-cleanup.pyz && sudo rm -rf aptsources-cleanup.pyz         
				;;
			     9)
				#Install Chromium
				echo "================ Установка Chromium ================"
				sudo apt install -y chromium chromium-l10n chromium-sandbox                                 
				wget https://www.dropbox.com/sh/qkm84kodwizr3q4/AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0 && \
				unzip -q 'AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0'
                                chmod +x Chromium/chromium.desktop
                                cp Chromium/chromium.desktop ~/.local/share/applications/  
                                sudo cp Chromium/Chromium.png /usr/share/icons/ 
                                tar xvf Chromium/chromium.tar.gz   
                                cp -r chromium ~/.config/ && rm -rf chromium     
				cp Chromium/ChromiumUninstall.sh ~/'Desktop'/Uninstall             
                                rm -rf 'AAB2tzdNJ4g7oB_Vu68Zy-1Wa?dl=0' && rm -Rf Chromium                                           
				;; 
			   10) 
				#Install Opera
		                echo "================ Установка Opera ================"
				sudo apt install -y opera-stable
                                wget https://www.dropbox.com/sh/f49m4vq27ytehz4/AACfwkqNO6CcizvhJElUIsqIa?dl=0 && \
				unzip -q 'AACfwkqNO6CcizvhJElUIsqIa?dl=0'		
                                chmod +x Opera/opera.desktop
                                cp Opera/opera.desktop ~/.local/share/applications/ 
                                sudo cp Opera/Opera.svg /usr/share/icons/      
                                tar xvf Opera/opera.tar.gz    
                                mv opera ~/.config/opera     
				cp Opera/OperaUninstall.sh ~/'Desktop'/Uninstall      
                                rm -rf 'AACfwkqNO6CcizvhJElUIsqIa?dl=0' && rm -Rf Opera                                
				;;
			   11)
				#Install Vivaldi       
                                echo "================ Установка Vivaldi ================"   
                                sudo apt install -y  vivaldi-stable                                                
				wget https://www.dropbox.com/sh/2en0ti85y9mr4zc/AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0 && \
				unzip -q 'AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0'				
				tar xvf Vivaldi/vivaldi-snapshot.tar.gz
				mv vivaldi-snapshot ~/.config/vivaldi	
				cp Vivaldi/VivaldiUninstall.sh ~/'Desktop'/Uninstall							
				rm -rf 'AADXX_KNMM9t-vQnl-Gd_ewMa?dl=0' && rm -Rf Vivaldi                                 
				;;
			   12)
                                #Install Min
				echo "================ Установка Min ================"
				sudo apt install -y min
                                wget https://www.dropbox.com/sh/5179t2mpm71b685/AADn5KYTquvcyQf_o-OAlHQNa?dl=0 && \
				unzip -q 'AADn5KYTquvcyQf_o-OAlHQNa?dl=0'                                                                                                        
				tar xvf MinAplPng/Min.tar.gz
				mv Min ~/.config/Min	
				cp MinAplPng/MinUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AADn5KYTquvcyQf_o-OAlHQNa?dl=0' && rm -Rf MinAplPng		 
				;;
			   13)
				#Install Brave
		                echo "================ Установка Brave ================"
				sudo apt install -y brave-browser
                                wget https://www.dropbox.com/sh/6g531ht3q5nt9hd/AAAh45XaZU9LhSqLTojHkWkRa?dl=0 && \
				unzip -q 'AAAh45XaZU9LhSqLTojHkWkRa?dl=0' 
				tar xvf Brave/BraveSoftware.tar.gz
				mv BraveSoftware ~/.config/BraveSoftware
				cp Brave/BraveUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAh45XaZU9LhSqLTojHkWkRa?dl=0' && rm -Rf Brave
				;;
			   14)
				#Install TorBrowser
				echo "================ Установка TorBrowser ================"      
				sudo apt install -y tor-browser	 
				wget https://www.dropbox.com/sh/3zfwfebyq47t7td/AAB8BZOjc6G66GYuODc6KCi8a?dl=0 && \
				unzip -q 'AAB8BZOjc6G66GYuODc6KCi8a?dl=0'
				cp Tor/TorBrowserUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAB8BZOjc6G66GYuODc6KCi8a?dl=0' && rm -Rf Tor
				;;
			   15)
				#Install LibreWolf
				echo "================ Установка LibreWolf =============="  
				sudo apt install -y librewolf
                                wget https://www.dropbox.com/sh/xdh6sx560o105lg/AABIuMvChhQ2DrYk6PFJxrSUa?dl=0 && \
				unzip -q 'AABIuMvChhQ2DrYk6PFJxrSUa?dl=0'    				                                                                         
				tar xvf LibreWolfApp/librewolf.tar.gz
				cp LibreWolfApp/LibreWolfUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AABIuMvChhQ2DrYk6PFJxrSUa?dl=0' && rm -Rf LibreWolfApp	                    
				;;
			   16)
				#Install Gvidm
				echo "================ Установка Gvidm =============================="
				sudo apt install -y gvidm
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/GvidmAplPng.tar.gz
				tar xvf GvidmAplPng.tar.gz && rm -rf GvidmAplPng.tar.gz 			 
                                chmod +x GvidmAplPng/gvidm.desktop
                                cp GvidmAplPng/gvidm.desktop ~/.local/share/applications/
				sudo cp GvidmAplPng/Gvidm.png /usr/share/icons/   
				cp GvidmAplPng/GvidmUninstall.sh ~/'Desktop'/Uninstall                       
				rm -Rf GvidmAplPng                                                                                                                                 
				;;
			   17)
				#Install Qshutdown
			        echo "================ Установка Qshutdown ================"
                                sudo apt install qshutdown -y
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/QshutdownAplPng.tar.gz
				tar xvf QshutdownAplPng.tar.gz && rm -rf QshutdownAplPng.tar.gz
                                chmod +x QshutdownAplPng/qshutdown.desktop
                                cp QshutdownAplPng/qshutdown.desktop ~/.local/share/applications/
                                sudo cp QshutdownAplPng/Qshutdown.png /usr/share/icons/
				cp QshutdownAplPng/QshutdownUninstall.sh ~/'Desktop'/Uninstall
				rm -Rf QshutdownAplPng  
				;;
			   18)
				#Install Bucklespring
			        echo "================ Установка Bucklespring ================"
			        sudo apt install -y bucklespring     
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/BucklespringAplPng.tar.gz
				tar xvf BucklespringAplPng.tar.gz && rm -rf BucklespringAplPng.tar.gz
                                chmod +x BucklespringAplPng/Bucklespring.desktop         
                                cp BucklespringAplPng/Bucklespring.desktop ~/.local/share/applications/            
                                sudo cp BucklespringAplPng/Bucklespring.desktop /etc/xdg/autostart/
				sudo cp BucklespringAplPng/Bucklespring.png /usr/share/icons/  
				cp BucklespringAplPng/BucklespringUninstall.sh ~/'Desktop'/Uninstall     
				rm -rf BucklespringAplPng/                               
				;;
			   19)
				#Install ThunarOfRoot-Работа с архивами в xfce4
			        echo "================ Установка ThunarOfRoot-Работа с архивами в xfce4 ================"
                                sudo apt install -y ark engrampa file-roller p7zip atool minizip zenity lzma pdlzip pbzip2 r-cran-zip rzip \
				xarchiver rar unar unrar-free thunar-archive-plugin youtube-dl ffmpeg                                                     
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/ThunarOfRoot.tar.gz && tar xvf ThunarOfRoot.tar.gz 
                                tar xvf ThunarOfRoot/ThunarOfRoot.desktop.tar.gz                         
                                chmod +x ThunarOfRoot.desktop
                                mv ThunarOfRoot.desktop ~/.local/share/applications/ 
                                tar xvf ThunarOfRoot/ThunarMousepad.tar.gz
                                cp -R Thunar ~/.config/ && rm -Rf Thunar
                                tar xvf ThunarOfRoot/youtube2what.tar.gz   
				chmod +x youtube2what 
				sudo mv youtube2what /usr/local/bin/
                                tar xvf ThunarOfRoot/icons.tar.gz
                                chmod -R 777 icons
                                sudo cp -R icons /usr/share/ && rm -Rf icons
                                echo "===== Mousepad ==================="
                                sudo apt install -y mousepad 
                                tar xvf ThunarOfRoot/MousepadApl.tar.gz				 
                                chmod +x MousepadApl/org.xfce.mousepad.desktop
                                cp MousepadApl/org.xfce.mousepad.desktop ~/.local/share/applications/
				rm -Rf MousepadApl       
				cp ThunarOfRoot/ThunarOfRootUninstall.sh ~/'Desktop'/Uninstall                         
                                rm -rf ThunarOfRoot.tar.gz && rm -Rf ThunarOfRoot                                                                                                                         				                                                                				 			                        
				;;
			   20)
				#Install CompizEmerald
			        echo "================ Установка CompizEmerald ================"
                                sudo apt install -y compiz compiz-core compiz-plugins compiz-plugins-default compiz-plugins-extra \
				compiz-plugins-main compizconfig-settings-manager emerald emerald-themes
		                wget https://gitlab.com/Chebur70/Setting/-/raw/main/CompizEmeraldAplPng.tar.gz
				tar xvf CompizEmeraldAplPng.tar.gz && rm -rf CompizEmeraldAplPng.tar.gz
				chmod +x CompizEmeraldAplPng/*.desktop                             
				cp CompizEmeraldAplPng/Compiz_off.desktop ~/.local/share/applications/
				cp CompizEmeraldAplPng/Compiz_on.desktop ~/.local/share/applications/                                
                                cp CompizEmeraldAplPng/ccsm.desktop ~/.local/share/applications/
				cp CompizEmeraldAplPng/Emerald_on.desktop ~/.local/share/applications/  
                                cp CompizEmeraldAplPng/emerald-theme-manager.desktop ~/.local/share/applications/                                                                                              
                                #cp CompizEmeraldAplPng/Emerald_on.desktop ~/.config//autostart/ && \
				#cp CompizEmeraldAplPng/Compiz_on.desktop ~/.config//autostart/  
                                chmod -R 777 CompizEmeraldAplPng/Compiz              
                                sudo cp -R CompizEmeraldAplPng/Compiz /usr/share/icons/          
				tar xvf CompizEmeraldAplPng/compiz.tar.gz  
                                mv compiz ~/.config/compiz      
				tar xvf CompizEmeraldAplPng/emerald.tar.gz
				cp CompizEmeraldAplPng/CompizEmeraldUninstall.sh ~/'Desktop'/Uninstall
				rm -Rf CompizEmeraldAplPng          
				;;
			  21)
				#Install Stacer
			        echo "================ Установка Stacer ================"
                                sudo apt install -y stacer       
				wget https://www.dropbox.com/sh/6db8pm1e530401b/AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0 && \
				unzip -q 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0'
				cp Bleachbit/StacerUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0' && rm -Rf Bleachbit
				;;
			  22)
                                #Install BleachBit
			        echo "================ Установка BleachBit ================"   
                                sudo apt install -y bleachbit  
                                wget https://www.dropbox.com/sh/6db8pm1e530401b/AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0 && \
				unzip -q 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0'
                                chmod +x Bleachbit/bleachbit-root.desktop
			        cp Bleachbit/bleachbit-root.desktop ~/.local/share/applications/
                                sudo cp Bleachbit/Bleachbit.png /usr/share/icons/
				cp Bleachbit/BleachBitUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AADO3fX5Jw1HNYt1WH9aAPJpa?dl=0' && rm -Rf Bleachbit          	 
			       ;;
			  23)
			        #Install Gkrellm
			        echo "================ Установка Gkrellm ================"
			        sudo apt install -y gkrellm 
				wget https://www.dropbox.com/sh/wafutq4xlczbza0/AAAy1l-wqCiLabOmhD82uCN8a?dl=0 && \
				unzip -q 'AAAy1l-wqCiLabOmhD82uCN8a?dl=0'
                                chmod +x Gkrellm/gkrellm.desktop
                                cp Gkrellm/gkrellm.desktop ~/.config/autostart/
				tar xvf Gkrellm/Bgkrellm2.tar.gz
				cp Gkrellm/GkrellmUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAy1l-wqCiLabOmhD82uCN8a?dl=0' && rm -Rf Gkrellm                                                                
			       ;;
			  24)
			        #Install RamboxAppimage
			        echo "================ Установка RamboxAppimage ================"	
				wget https://www.dropbox.com/sh/4j27qqksnmsq9wn/AADjG8vhqn64jId8QeazMOdRa?dl=0 && \
				unzip -q 'AADjG8vhqn64jId8QeazMOdRa?dl=0'
                                chmod +x Rambox/RamboxApp.desktop
				cp Rambox/RamboxApp.desktop ~/.local/share/applications/
				sudo cp Rambox/Rambox.png /usr/share/icons/
				chmod +x ./Rambox/*.AppImage
                                sudo mv Rambox/Rambox*.AppImage /opt/Rambox.AppImage
				cp Rambox/RamboxUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AADjG8vhqn64jId8QeazMOdRa?dl=0' && rm -Rf Rambox                                           
			       ;;
			  25)
			        #Install FranzAppimage
				echo "================ Установка FranzAppimage ================"
                                wget https://www.dropbox.com/sh/pm6hq9npw8jahvw/AADwWMEDOrwkht9unmc9vExYa?dl=0 && \
				unzip -q 'AADwWMEDOrwkht9unmc9vExYa?dl=0'
                                chmod +x Franz/FranzApp.desktop
				cp Franz/FranzApp.desktop ~/.local/share/applications/
				sudo cp Franz/FranzApp.png /usr/share/icons/
				chmod +x ./Franz/*.AppImage
                                sudo mv Franz/Franz*.AppImage /opt/Franz.AppImage
				cp Franz/FranzUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AADwWMEDOrwkht9unmc9vExYa?dl=0' && rm -Rf Franz                                                             
			       ;;
			  26)
			        #Install SkypeWeb
		                echo "================ Установка SkypeWeb ================"
				wget https://www.dropbox.com/sh/d15l1f1brdh8608/AAAdFTVBEeM66CReApIlLCjRa?dl=0 && \
				unzip -q 'AAAdFTVBEeM66CReApIlLCjRa?dl=0'
                                chmod +x Skype/SkypeF.desktop
				cp Skype/SkypeF.desktop ~/.local/share/applications/
				sudo cp Skype/Skype.png /usr/share/icons/
				cp Skype/SkypeUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAAdFTVBEeM66CReApIlLCjRa?dl=0' && rm -Rf Skype
			       ;;
		          27)
			        #Install Zoom.AppImage
				echo "================ Установка Zoom.AppImage ================"
				wget https://www.dropbox.com/sh/9t72pow785wpceu/AAAScxcu5xmrmup57YfakUQsa?dl=0 && \
				unzip -q 'AAAScxcu5xmrmup57YfakUQsa?dl=0'
                                chmod +xZoom/ZoomApp.desktop
				cp Zoom/ZoomApp.desktop ~/.local/share/applications/
				sudo cp Zoom/Zoom.png /usr/share/icons/
				chmod +x ./Zoom/*.AppImage
                                sudo mv Zoom/Zoom*.AppImage /opt/Zoom.AppImage
				cp Zoom/ZoomUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAAScxcu5xmrmup57YfakUQsa?dl=0' && rm -Rf Zoom
			       ;;
			  28)
			        #Install Telegram
		                echo "================ Установка Telegram ================"
                                sudo apt install -y telegram-desktop
				wget https://www.dropbox.com/sh/s3ybf17voaga97i/AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0 && \
				unzip -q 'AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0'
				cp TelegraM/org.telegram.desktop.desktop ~/.local/share/applications/
				sudo cp TelegraM/Telegram.png /usr/share/icons/
				cp TelegraM/TelegramUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAD1q9E-oOOZ39h7o_RpPZ_ca?dl=0' && rm -Rf TelegraM
		               ;;
			  29)
			        #Install SignalApt
				echo "================  Установка SignalApt ================"				
                                sudo apt install -y signal-desktop
				wget https://www.dropbox.com/sh/nszgf7iaeqluw33/AABeHiSD0fTZWifAOGWnWx01a?dl=0 && \
				unzip -q 'AABeHiSD0fTZWifAOGWnWx01a?dl=0'
				chmod +x Signal/signal-desktop.desktop
			        cp Signal/signal-desktop.desktop ~/.local/share/applications/
                                sudo cp Signal/Signal.png /usr/share/icons/	
				cp Signal/SignalUninstall.sh ~/'Desktop'/Uninstall			
                                rm -rf 'AABeHiSD0fTZWifAOGWnWx01a?dl=0' rm -Rf Signal
			       ;;
			  30)
			        #Install WireWeb
		                echo "================ Установка WireWeb ================"
                                wget https://www.dropbox.com/sh/cchy00tljr431il/AABPjdODeAHavyraKdqF91-ba?dl=0 && \
				unzip -q 'AABPjdODeAHavyraKdqF91-ba?dl=0'
                                chmod +x Wire/WireF.desktop
				cp Wire/WireF.desktop ~/.local/share/applications/
				sudo cp Wire/Wire.png /usr/share/icons/
				cp Wire/WireF.desktop ~/.local/share/applications/
				cp Wire/WireUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AABPjdODeAHavyraKdqF91-ba?dl=0' && rm -Rf Wire                    
			       ;;
                          31)
			        #Install WhatsAppDeb
		                echo "================ Установка WhatsAppDeb ================"
                                sudo apt install -y 'whatsapp-for-linux'
                                wget https://www.dropbox.com/sh/itnkex987dv63jh/AADCkyWHaZq9cfhBb0QBghlqa?dl=0 && \
				unzip -q 'AADCkyWHaZq9cfhBb0QBghlqa?dl=0'
				chmod +x Whatsapp/com.github.eneshecan.WhatsAppForLinux.desktop
				cp Whatsapp/com.github.eneshecan.WhatsAppForLinux.desktop ~/.local/share/applications/
                                sudo cp Whatsapp/Whatsapp.png /usr/share/icons/   
				cp Whatsapp/WhatsAppUninstall.sh ~/'Desktop'/Uninstall                                                      
				rm -rf 'AADCkyWHaZq9cfhBb0QBghlqa?dl=0' && rm -Rf Whatsapp                            
			       ;;
			  32)
		                #Install ViberAppImage
                                echo "================ Установка ViberAppImage ================"
                                wget https://www.dropbox.com/sh/5c88ucnin6uae0h/AABQCi4foO5jVO-9AXS-unXea?dl=0 && \
				unzip -q 'AABQCi4foO5jVO-9AXS-unXea?dl=0'
                                chmod +x Viber/ViberApp.desktop
				cp Viber/ViberApp.desktop ~/.local/share/applications/
				sudo cp Viber/Viber.png /usr/share/icons/
				chmod +x ./Viber/*.AppImage
                                sudo mv Viber/viber.AppImage /opt/
				cp Viber/ViberUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AABQCi4foO5jVO-9AXS-unXea?dl=0' && rm -Rf Viber
			       ;;
			  33)
			        #Install SMS-Messages
				echo "================  Установка SMS-Messages ================"
                                wget https://www.dropbox.com/sh/yqw1z8v8fexxrho/AADV8N4OEgkmQxbxz70yvNBea?dl=0 && \
				unzip -q 'AADV8N4OEgkmQxbxz70yvNBea?dl=0'
                                chmod +x SMSMessages/SMSMessagesF.desktop
				cp SMSMessages/SMSMessagesF.desktop ~/.local/share/applications/
				sudo cp SMSMessages/SMSMessages.png /usr/share/icons/
				cp SMSMessages/SMS-MessagesUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AADV8N4OEgkmQxbxz70yvNBea?dl=0' && rm -Rf SMSMessages             		              
			       ;;
			  34)
			        #Install GoogleTranslateWeb
			        echo "================ Установка GoogleTranslateWeb ================"
                                wget https://www.dropbox.com/sh/fj0l3tn6crbsfyg/AABqfHbT3o40SvyfD9yprFD-a?dl=0 && \
				unzip -q 'AABqfHbT3o40SvyfD9yprFD-a?dl=0'
                                chmod +x GoogleTranslate/GoogleTranslateF.desktop
				cp GoogleTranslate/GoogleTranslateF.desktop ~/.local/share/applications/
				sudo cp GoogleTranslate/GoogleTranslate.png /usr/share/icons/    
				cp GoogleTranslate/GoogleTranslateUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AABqfHbT3o40SvyfD9yprFD-a?dl=0' && rm -Rf GoogleTranslate	                                            
			       ;;
			  35)
			        #Install SpeedtestWeb
		                echo "================ Установка SpeedtestWeb ================"
                                wget https://www.dropbox.com/sh/vysj5fkvnxkmeii/AAA5l_9P6bGWtOCMj0ng4nD1a?dl=0 && \
				unzip -q 'AAA5l_9P6bGWtOCMj0ng4nD1a?dl=0'
                                chmod +x Speedtest/SpeedtestF.desktop
				cp Speedtest/SpeedtestF.desktop ~/.local/share/applications/
				sudo cp Speedtest/Speedtest.png /usr/share/icons/
				cp Speedtest/SpeedtestUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAA5l_9P6bGWtOCMj0ng4nD1a?dl=0' && rm -Rf Speedtest	  
			       ;;
			  36)
			        #Install AppImageUpdate
				echo "================ Установка AppImageUpdate ================"      
				wget https://www.dropbox.com/sh/0jjoic2u2qiu2gl/AABso4fr297VkkQEuGcFuZz3a?dl=0 && \
				unzip -q 'AABso4fr297VkkQEuGcFuZz3a?dl=0'			
				chmod +x AppImageUpdate/AppImageUpdate.desktop
				cp AppImageUpdate/AppImageUpdate.desktop  ~/.local/share/applications/
				sudo cp AppImageUpdate/AppImageUpdate.png /usr/share/icons/
				chmod +x ./AppImageUpdate/*.AppImage
				sudo mv AppImageUpdate/AppImageUpdate*.AppImage  /opt/AppImageUpdate.AppImage	
				cp AppImageUpdate/AppImageUpdateUninstall.sh ~/'Рабочий стол'/Uninstall 		
				rm -rf 'AABso4fr297VkkQEuGcFuZz3a?dl=0' && rm -Rf AppImageUpdate
		 	       ;;			
			  37)
			        #Install BauhAppImage
			        echo "================ Установка BauhAppImage ================"	                               
				wget https://www.dropbox.com/sh/3o0is46szlbc7jp/AABXRmQ_h8fNx6uAFbpj84dua?dl=0 && \
				unzip -q 'AABXRmQ_h8fNx6uAFbpj84dua?dl=0'                                             
                                chmod +x Bauh/BauhApp.desktop
                                cp Bauh/BauhApp.desktop ~/.local/share/applications/   
                                sudo cp Bauh/Bauh.svg /usr/share/icons/ 
                                chmod +x ./Bauh/*.AppImage
				sudo mv Bauh/bauh*.AppImage /opt/bauh.AppImage       
				cp Bauh/BauhUninstall.sh ~/'Desktop'/Uninstall            
                                rm -rf 'AABXRmQ_h8fNx6uAFbpj84dua?dl=0' && rm -Rf Bauh
			       ;;
			  38)
			        #Install AnydeskDeb
				echo "================ Установка AnydeskDeb ================"
                                wget https://www.dropbox.com/sh/1vvd99olkmtraih/AACM_x29BDmbtW7CtaglwFwfa?dl=0 && \
				unzip -q 'AACM_x29BDmbtW7CtaglwFwfa?dl=0'
                                sudo dpkg -i AnyDesk/*.deb
                                sudo apt install -fy	
				cp AnyDesk/AnydeskUninstall.sh ~/'Desktop'/Uninstall                                 
                                rm -rf 'AACM_x29BDmbtW7CtaglwFwfa?dl=0' && rm -Rf AnyDesk			 			 
			       ;;
			  39)
			        #Install DetectItEasyDeb
				echo "================ Установка DetectItEasyDeb================"
				wget https://www.dropbox.com/sh/530b2baiwt6865n/AADPn5dNjksXn7uexKcmxvCfa?dl=0 && \
				unzip -q 'AADPn5dNjksXn7uexKcmxvCfa?dl=0'                               
                                sudo dpkg -i DetectItEasy/*.deb
                                sudo apt install -fy     
				cp DetectItEasy/DetectItEasyUninstall.sh ~/'Desktop'/Uninstall 
                                rm -rf 'AADPn5dNjksXn7uexKcmxvCfa?dl=0' && rm -Rf DetectItEasy 
			       ;;
			  40)
			        #Install FreeTube
		                echo "================ Установка FreeTube ================"
                                sudo apt-get install -y freetube                                                  
                                wget https://www.dropbox.com/sh/n78t9ogufcpbiw0/AAB7HYdU94o_7BKIP1KUPGjWa?dl=0 && \
				unzip -q 'AAB7HYdU94o_7BKIP1KUPGjWa?dl=0'
				cp reeTubeApp/FreeTubeUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAB7HYdU94o_7BKIP1KUPGjWa?dl=0' && rm -Rf FreeTubeApp
			       ;;
			  41)
			        #Install Cherrytree
			        echo "================ Установка Cherrytree ================"
				sudo apt-get install -y cherrytree                                                            				
				wget https://www.dropbox.com/sh/ev9n5ttb3cf99u7/AAAWZl4luvIIk3BJEfX-eRTWa?dl=0 && \
				unzip -q 'AAAWZl4luvIIk3BJEfX-eRTWa?dl=0'                   				                                
                                chmod -R 777 Cherrytree                         
                                sudo cp Cherrytree/Cherrytree.svg /usr/share/icons/  
                                cp Cherrytree/cherrytree.desktop ~/.local/share/applications/
				cp Cherrytree/ПаролиLiveссылки.ctb ~/'Desktop'/
                                cp -r Cherrytree/cherrytree ~/.config/  
				cp Cherrytree/CherrytreeUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAWZl4luvIIk3BJEfX-eRTWa?dl=0' && rm -Rf Cherrytree
			       ;;
			  42)
                               #Install MxBootRepairAppimage
			       echo "================ Установка MxBootRepairAppimage ================"
			       wget https://www.dropbox.com/sh/90uazf02iu0qc4o/AADREKkXv7VF6R-espdXDbeYa?dl=0 && \
			       unzip -q 'AADREKkXv7VF6R-espdXDbeYa?dl=0'	
			       chmod +x ./MxBootRepair/*.AppImage
                               sudo cp MxBootRepair/MXBootRepair.AppImage /opt/		       
			       chmod +x MxBootRepair/MxBootRepair.desktop
			       cp MxBootRepair/MxBootRepair.desktop ~/.local/share/applications/
			       sudo cp MxBootRepair/MxBootRepair.png /usr/share/icons/
			       cp MxBootRepair/MXBootRepairUninstall.sh ~/'Desktop'/Uninstall
			       rm -rf 'AADREKkXv7VF6R-espdXDbeYa?dl=0' && rm -Rf MxBootRepair
			       ;;
			  43)
				#Install MXBootOptionsAppimage
				echo "================ Установка MXBootOptionsAppimage ================"
				wget https://www.dropbox.com/sh/sppi7c0e1cy8r42/AAAhlc194CCNoCJkZ5vClkpua?dl=0 && \
				unzip -q 'AAAhlc194CCNoCJkZ5vClkpua?dl=0'
				chmod +x MXBoot/MXBootOptions.AppImage	
				sudo cp MXBoot/MXBootOptions.AppImage /opt/
				chmod +x MXBoot/MXBootOptions.desktop
                                cp MXBoot/MXBootOptions.desktop ~/.local/share/applications/
                                sudo cp MXBoot/MXBootOptions.png /usr/share/icons/ 
				cp MXBoot/MXBootOptionsUninstall.sh ~/'Desktop'/Uninstall			                                
				rm -rf 'AAAhlc194CCNoCJkZ5vClkpua?dl=0' && rm -Rf MXBoot 
				;;
			   44)
				#Install MXMenuEditorAppimage
				echo "================ Установка MXMenuEditorAppimage ================"
				wget https://www.dropbox.com/sh/qmn5jr7364ig42j/AAC0yA8V5bs8alDmANaMJX0Ca?dl=0 && \
				unzip -q 'AAC0yA8V5bs8alDmANaMJX0Ca?dl=0'
                                chmod +x MXMenu/MXMenuEditor.AppImage	
				sudo cp MXMenu/MXMenuEditor.AppImage /opt/
				chmod +x MXMenu/MXMenuEditor.desktop
                                cp MXMenu/MXMenuEditor.desktop ~/.local/share/applications/
                                sudo cp MXMenu/MXMenuEditor.png /usr/share/icons/ 
				cp MXMenu/MXMenuEditorUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAC0yA8V5bs8alDmANaMJX0Ca?dl=0' && rm -Rf MXMenu
				;;
			   45)
				#Install ddCopy
			        echo "================ Установка ddCopy ================"                            
			        wget https://www.dropbox.com/sh/t4b3rja97a04tsc/AAAEyWsv5YAGcujFRnX6vGjYa?dl=0 && \
				unzip -q 'AAAEyWsv5YAGcujFRnX6vGjYa?dl=0'
			        sudo dpkg -i ddCopy/ddcopy*.deb
			        sudo apt install -fy  
				cp ddCopy/ddCopyDebUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAEyWsv5YAGcujFRnX6vGjYa?dl=0' && rm -Rf ddCopy                             
				;;
			   46)
				#Install Unetbootin 
			        echo "================ Установка Unetbootin ================"
                                sudo apt install -y xclip mtools extlinux
                                wget https://www.dropbox.com/sh/71p463jvkqkzmpz/AACuBEC58iO1vLxoUz5FuZKOa?dl=0 && \
				unzip -q 'AACuBEC58iO1vLxoUz5FuZKOa?dl=0'			                                
                                chmod +x Unetbootin/unetbootin*.bin
                                sudo mv Unetbootin/unetbootin*.bin /opt/unetbootin.bin                                				 
				chmod +x Unetbootin/Unetbootin.desktop                                                
				cp Unetbootin/Unetbootin.desktop ~/.local/share/applications/
                                sudo cp Unetbootin/Unetbootin.png /usr/share/icons/   
				cp Unetbootin/UnetbootinUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AACuBEC58iO1vLxoUz5FuZKOa?dl=0' && rm -Rf Unetbootin                                       			 
				;;
			   47)
			        #Install  Etcher
			        echo "================ Установка Etcher ================"
			        sudo apt install -y balena-etcher 
				wget https://www.dropbox.com/sh/dajkpg6rwof3ru2/AACn4uvLuERyraJVZNio42nAa?dl=0 && \
				unzip -q 'AACn4uvLuERyraJVZNio42nAa?dl=0'
				cp Etcher/EtcherUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AACn4uvLuERyraJVZNio42nAa?dl=0' && rm -Rf Etcher       		         
			       ;;
			  48)
			        #Install RosaImageWriter
                                echo "================ Установка RosaImageWriter ================"
				wget https://www.dropbox.com/sh/dr3j9cee5db2wk6/AABV9sSW_anWxoZMqws9iLqoa?dl=0 && \
				unzip -q 'AABV9sSW_anWxoZMqws9iLqoa?dl=0'                               
                                chmod +x ./RosaIW/RosaImageWriter
                                sudo cp RosaIW/RosaImageWriter /opt/                                
                                chmod +x RosaIW/RosaImageWriter.desktop
                                cp RosaIW/RosaImageWriter.desktop ~/.local/share/applications/
                                sudo cp RosaIW/RosaImageWriter.png /usr/share/icons/  
				cp RosaIW/RosaImageWriterUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AABV9sSW_anWxoZMqws9iLqoa?dl=0' && rm -Rf RosaIW
			       ;;			
			  49)
			        #Install DDRescue-GUI
			        echo "================ Установка DDRescue-GUI ================"  
                                wget https://www.dropbox.com/sh/wzzgx3j35lziyhz/AABapMGhtZVW3KvRec8gcbB2a?dl=0 && \
				unzip -q 'AABapMGhtZVW3KvRec8gcbB2a?dl=0'
                                sudo dpkg -i DDRescueGUI/*.deb
				sudo apt install -fy                                                                               
				cp DDRescueGUI/DDRescueGUIUninstall.sh ~/'Рабочий стол'/Uninstall 
                                rm -rf 'AABapMGhtZVW3KvRec8gcbB2a?dl=0' && rm -Rf DDRescueGUI                             
			       ;;
		          50)
                                #Install MultiBootUSB
				echo "================ Установка MultiBootUSB ================"
                                wget https://www.dropbox.com/sh/tfngu17tt3dnj4p/AADNU6lEkYmi35rFMJPsNVeRa?dl=0 && \
				unzip -q 'AADNU6lEkYmi35rFMJPsNVeRa?dl=0'
                                sudo dpkg -i MultiBootUSB/python3-multibootusb*.deb
				sudo apt install -fy                                 					 
                                chmod +x MultiBootUSB/multibootusb.desktop                                                              
                                cp MultiBootUSB/multibootusb.desktop ~/.local/share/applications/
                                sudo cp MultiBootUSB/MultiBootUsb.svg /usr/share/icons/   
				cp MultiBootUSB/MultiBootUSBUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AADNU6lEkYmi35rFMJPsNVeRa?dl=0' && rm -Rf MultiBootUSB 
			       ;;
			  51)
			        #Install Ventoy
			        echo "================ Установка Ventoy ================"  
				sudo apt install -y ventoy  
				wget https://www.dropbox.com/sh/8xszka2m3dtyugy/AAD7pz3NICDHsOMkungofVeCa?dl=0 && \
				unzip -q 'AAD7pz3NICDHsOMkungofVeCa?dl=0' 
				cp VentoyWeb/VentoyWebUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAD7pz3NICDHsOMkungofVeCa?dl=0' && rm -Rf VentoyWeb                      			 
			       ;;
			  52)
			        #Install Mintstick
		                echo "================ Установка Mintstick ======================"
                                wget https://www.dropbox.com/sh/6bz3gmomboihu7j/AACErhxW5xoPNi3vQEX3hq0ua?dl=0 && \
				unzip -q 'AACErhxW5xoPNi3vQEX3hq0ua?dl=0'                               
                                sudo dpkg -i Mintstick/*.deb
		                sudo apt install -f -y                                        
                                chmod +x Mintstick/*.desktop
                                cp Mintstick/mintstick.desktop ~/.local/share/applications/
				cp Mintstick/mintstick-format.desktop ~/.local/share/applications/  				
				chmod +x Mintstick/mintstick
                                sudo cp -R Mintstick/mintstick /usr/share/icons/                                                            				  
				chmod +x Mintstick/sysctl.conf
                                sudo cp Mintstick/sysctl.conf /etc/sysctl.conf 
				cp Mintstick/MintstickUninstall.sh ~/'Desktop'/Uninstall
				echo "mintstick hold" | sudo dpkg --set-selections
                                rm -rf 'AACErhxW5xoPNi3vQEX3hq0ua?dl=0' && rm -Rf Mintstick                        		       				                                                                   				 
			       ;;
			  53)
			        #Install CapacityTester   
			        echo "================ Установка CapacityTester ================"
			        wget https://www.dropbox.com/sh/61u0gag8i9au1m1/AAAzprmaVblT-j44jUnNtgVAa?dl=0 && \
				unzip -q 'AAAzprmaVblT-j44jUnNtgVAa?dl=0'
				chmod +x CapacityTester/*.AppImage  
				sudo mv CapacityTester/Capacity_Tester*.AppImage /opt/CapacityTester                                                                 
				chmod +x CapacityTester/CapacityTester.desktop                                                                                                                                                                          
                                cp CapacityTester/CapacityTester.desktop ~/.local/share/applications/
                                sudo cp CapacityTester/CapacityTester.svg /usr/share/icons/
				cp CapacityTester/CapacityTesterUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAAzprmaVblT-j44jUnNtgVAa?dl=0' && rm -Rf CapacityTester                                                            
			       ;;
			  54)
			        #Install MKUSB
			        echo "================ Установка MKUSB ================"  
                                sudo apt install -y mkusb
				wget https://www.dropbox.com/sh/cs1qgwwepv1dcbp/AAC1PNzEoioRAE4VQKV1sXbna?dl=0 && \
				unzip -q 'AAC1PNzEoioRAE4VQKV1sXbna?dl=0'
				cp MKUSB/MKUSBUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAC1PNzEoioRAE4VQKV1sXbna?dl=0' && rm -Rf MKUSB  		       
			       ;;
			  55)
			        #Install LiveUSBMakerQtAppImage   
			        echo "================ Установка LiveUSBMakerQtAppImage ================"
			        wget https://www.dropbox.com/sh/4ni1snhnlq6ejcd/AACXlV8GNSyoObTKxFyb2_caa?dl=0 && \
				unzip -q 'AACXlV8GNSyoObTKxFyb2_caa?dl=0'			 				                                                                 
				chmod +x LumQtApp/*.desktop                                                                                                                                                                          
                                cp LumQtApp/LiveUSBMakerQt.desktop ~/.local/share/applications/
                                sudo cp LumQtApp/LiveUSBMaker.png /usr/share/icons
				chmod +x ./LumQtApp/*.AppImage 
				sudo mv LumQtApp/live-usb-maker-qt*.AppImage /usr/local/bin/LiveUSBMakerQt.AppImage
				cp LumQtApp/LiveUSBMakerQtAppImage.png ~/'Desktop'/
				cp LumQtApp/DDLiveUSBMakerUninstall.sh ~/'Desktop'/Uninstall 
				rm -rf 'AACXlV8GNSyoObTKxFyb2_caa?dl=0' && rm -Rf LumQtApp                    
			       ;;
			  56)
			        #Install LiveUSBMaker.AppImage   
			        echo "================ Установка LiveUSBMaker.AppImage ================"
			        wget https://www.dropbox.com/sh/4ni1snhnlq6ejcd/AACXlV8GNSyoObTKxFyb2_caa?dl=0 && \
				unzip -q 'AACXlV8GNSyoObTKxFyb2_caa?dl=0'			 				                                                                 
				chmod +x LumQtApp/*.desktop                                                                                                                                                                          
                                cp LumQtApp/LiveUSBMakerAppImage.desktop ~/.local/share/applications/
                                sudo cp LumQtApp/LiveUSBMaker.png /usr/share/icons
				chmod +x ./LumQtApp/*.AppImage 
				sudo cp LumQtApp/LiveUSBMaker.AppImage /usr/local/bin/
				cp LumQtApp/LiveUSBMakerAppImage.png ~/'Desktop'/
				cp LumQtApp/DDLiveUSBMakerUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AACXlV8GNSyoObTKxFyb2_caa?dl=0' && rm -Rf LumQtApp
			       ;;
			  57)
			        #Install DDLiveUsb.AppImage   
			        echo "================ Установка DDLiveUsb.AppImage ================"
			        wget https://www.dropbox.com/sh/4ni1snhnlq6ejcd/AACXlV8GNSyoObTKxFyb2_caa?dl=0 && \
				unzip -q 'AACXlV8GNSyoObTKxFyb2_caa?dl=0'			 				                                                                 
				chmod +x LumQtApp/*.desktop                                                                                                                                                                          
                                cp LumQtApp/DDLiveUsbAppImage.desktop ~/.local/share/applications/
                                sudo cp LumQtApp/DDLiveUsb.png /usr/share/icons
				chmod +x ./LumQtApp/*.AppImage 
				sudo cp LumQtApp/DDLiveUsb.AppImage /usr/local/bin/
				cp LumQtApp/DDLiveUsbAppImage.png ~/'Desktop'/
				cp LumQtApp/DDLiveUSBMakerUninstall.sh ~/'Desktop'/Uninstall 
				rm -rf 'AACXlV8GNSyoObTKxFyb2_caa?dl=0' && rm -Rf LumQtApp			       
			       ;;
			  58)
			        #Install DDLiveUSBSh+LiveUSBMakerSh
			        echo "================ Установка DDLiveUSBSh+LiveUSBMakerSh ================"   
				sudo apt-get install -y extlinux syslinux-common
				wget https://www.dropbox.com/sh/4ni1snhnlq6ejcd/AACXlV8GNSyoObTKxFyb2_caa?dl=0 && \
				unzip -q 'AACXlV8GNSyoObTKxFyb2_caa?dl=0'				      				                            
                                chmod +x LumQtApp/*.desktop
                                cp LumQtApp/DDLiveUsbSh.desktop ~/.local/share/applications/
				cp LumQtApp/LiveUSBMakerSh.desktop ~/.local/share/applications/
				tar xvf LumQtApp/DDLiveUSBMakerSh.tar.gz
				chmod -R 777 DDLiveUSBMakerSh/DDLiveUSBMakerSh
                                sudo cp -R  DDLiveUSBMakerSh/DDLiveUSBMakerSh /usr/share/icons/				 
				sudo mv DDLiveUSBMakerSh /usr/local/bin/DDLiveUSBMakerSh
				cp LumQtApp/DDLiveUsbAppImage.png ~/'Desktop'/
				cp LumQtApp/DDLiveUSBMakerUninstall.sh ~/'Desktop'/Uninstall    
                                rm -rf 'AACXlV8GNSyoObTKxFyb2_caa?dl=0' && rm -Rf LumQtApp
			       ;;
			  59)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                                
			       ;;
			  60)
			        #Install Дополнительные драйвера Wi-Fi broadcom
			        echo "================ Установка Дополнительные драйвера Wi-Fi broadcom ================" 
                                sudo apt install -y firmware-linux firmware-linux-nonfree broadcom-sta-dkms 
			       ;;
			  61)
			       #Install Дополнительные драйвера Wi-Fi atheros
				echo "================ Установка Wi-Fi atheros ================"
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-atheros         	                                
			       ;;
			  62)
			        #Install Дополнительные драйвера Wi-Fi realtek
                                echo "================ Установка Wi-Fi realtek ========================="
				sudo apt install -y firmware-linux firmware-linux-nonfree firmware-realtek 
			       ;;
			  63)
			        #Install Дополнительные драйвера Wi-Fi Intel
				echo "================ Установка Wi-Fi intel =========================="
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-iwlwifi                               
			       ;;
			  64)
			        #Install Дополнительные драйвера Wi-Fi Marvell и NXP (Libertas)
			        echo "================ Установка Wi-Fi Marvell и NXP (Libertas) ============="	
                                sudo apt install -y firmware-linux firmware-linux-nonfree firmware-libertas 				 
			       ;;
			  65)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"          
			       ;;
		          66)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                           
			       ;;
			  67)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================"                    
			       ;;
			  68)
                                #Install Cutter
			        echo "================ Установка Cutter ================"
                                wget https://www.dropbox.com/sh/q9umcsqs866sv8l/AACD2TXDiZ--vmanc23_xOeGa?dl=0 && \
				unzip -q 'AACD2TXDiZ--vmanc23_xOeGa?dl=0' 
                                chmod +x Cutter/Cutter*.AppImage
                                sudo mv Cutter/Cutter*.AppImage /opt/Cutter.AppImage                                                                       
                                chmod +x Cutter/CutterAppImage.desktop                                                                                                               
                                cp Cutter/CutterAppImage.desktop ~/.local/share/applications/
                                sudo cp Cutter/Cutter.png /usr/share/icons/  				
				tar xvf Cutter/rizin.tar.gz
				mv rizin ~/.config/rizin         
				cp Cutter/CutterUninstall.sh ~/'Desktop'/Uninstall        
                                rm -rf 'AACD2TXDiZ--vmanc23_xOeGa?dl=0' && rm -Rf Cutter			
			       ;;
			  69)
			        #Install Gotop
				echo "================ Установка Gotop ================"
                                wget https://www.dropbox.com/sh/d5ugl4dc43jcmxs/AADSkJBfQdCiJP4Io_8uNmcNa?dl=0 && \
				unzip -q 'AADSkJBfQdCiJP4Io_8uNmcNa?dl=0'
                                chmod +x Gotop/gotop				
				sudo cp Gotop/gotop /opt/                                 
				sudo cp Gotop/Gotop.png /usr/share/icons/                              
                                chmod +x Gotop/Gotop.desktop
                                cp Gotop/Gotop.desktop ~/.local/share/applications/           
				cp Gotop/GotopUninstall.sh ~/'Desktop'/Uninstall                                             				  
                                rm -rf 'AADSkJBfQdCiJP4Io_8uNmcNa?dl=0' && rm -Rf Gotop                            
			       ;;
			  70)
			        #Install GImageReader
			        echo "================ Установка GImageReader ================"
				sudo apt install -y gimagereader tesseract-ocr-osd tesseract-ocr-rus tesseract-ocr-eng
                                wget https://www.dropbox.com/sh/om8mkd01k6rouua/AADXp3fQj2SeYjiGURt9MpQIa?dl=0 && \
				unzip -q 'AADXp3fQj2SeYjiGURt9MpQIa?dl=0'
                                cp GImageReader/GImageReaderUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AADXp3fQj2SeYjiGURt9MpQIa?dl=0' && rm -Rf GImageReader
			       ;;
		          71)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 	                         	 				                                  
			       ;;
                          72)
			        #Install MOCP-MusicOnConsolePlayer
				echo "================ Установка MOCP-MusicOnConsolePlayer ================"
				sudo apt install -y moc moc-ffmpeg-plugin
				wget https://www.dropbox.com/sh/980q1ps3l86oi2a/AAAfyy_SABXnaMRDSNrrJ7uja?dl=0 && \
				unzip -q 'AAAfyy_SABXnaMRDSNrrJ7uja?dl=0'
				chmod +x MOCP/Moc.desktop
				cp MOCP/Moc.desktop ~/.local/share/applications/
				sudo cp MOCP/Moc.png /usr/share/icons/
				cp MOCP/Accept-AmamosLaVida.mp3 ~/'Desktop'/
				cp MOCP/MOCP-MusicOnConsolePlayer.sh ~/'Desktop'/
				cp MOCP/MOCPUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAAfyy_SABXnaMRDSNrrJ7uja?dl=0' && rm -Rf MOCP
                               ;;
                          73)
			        #Install Transmission
				echo "================ Установка Любая Ваша программа ================"
				wget https://www.dropbox.com/sh/y6xvctjm5r9a6o3/AABZEq7tq3rNX9O0RLaIppIza?dl=0 && \
				unzip -q 'AABZEq7tq3rNX9O0RLaIppIza?dl=0'
				sudo dpkg -i transmission/transmission*.deb
				sudo apt install -fy
				cp transmission/TransmissionUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AABZEq7tq3rNX9O0RLaIppIza?dl=0' && rm -Rf transmission 
                               ;;
                          74)
			        #Install QBittorrent
				echo "================ Установка Любая Ваша программа ================"
				sudo apt install -y qbittorrent
				wget https://www.dropbox.com/sh/83ftoqo026abqgg/AABm3SnbVcFxwL3kMhpIcB8da?dl=0 && \
				unzip -q 'AABm3SnbVcFxwL3kMhpIcB8da?dl=0'
				chmod +x QBittorrent/org.qbittorrent.qBittorrent.desktop
				cp QBittorrent/org.qbittorrent.qBittorrent.desktop ~/.local/share/applications/
				sudo cp QBittorrent/QBittorrent.png /usr/share/icons/
				cp QBittorrent/QBittorrentUninstall.sh ~/'Desktop'/Uninstall     
				rm -rf 'AABm3SnbVcFxwL3kMhpIcB8da?dl=0' && rm -Rf QBittorrent
                               ;;
                          75)
			       #Install Persepolis 
				echo "================ Установка Persepolis ================"	
                                sudo apt install -y aria2 persepolis
				wget https://www.dropbox.com/sh/t5zggq9dzns19ap/AAANV8d7XXMxzympXfUWMo31a?dl=0 && \
				unzip -q 'AAANV8d7XXMxzympXfUWMo31a?dl=0'
                                cp Persepolis/Persepolis.png ~/'Desktop'                                 
				tar xvf Persepolis/persepolis_download_manager.tar.gz
				mv persepolis_download_manager ~/.config/
				tar xvf Persepolis/persepolis.tar.gz
				cp Persepolis/PersepolisUninstall.sh ~/'Desktop'/Uninstall				
                                rm -rf 'AAANV8d7XXMxzympXfUWMo31a?dl=0' && rm -Rf Persepolis     
                               ;;
                          76)
			        #Install ClipGrab
			        echo "================ Установка ClipGrab ================"				 
                                sudo apt install -y clipgrab	
				wget https://www.dropbox.com/sh/jxap3clejvvii2e/AAC61QrPWcy7X9FbSVMnrVeGa?dl=0 && \
				unzip -q 'AAC61QrPWcy7X9FbSVMnrVeGa?dl=0'		         		       
				cp ClipGrabApp/ClipGrabUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAC61QrPWcy7X9FbSVMnrVeGa?dl=0' && rm -Rf ClipGrabApp	       		 			       
                               ;;
                          77)
			        #Install XDM
				echo "================ Установка XDM ================"
				wget https://www.dropbox.com/sh/9xwhut480sofbjd/AAAdfIqtIhWjOdOKPMG76RL0a?dl=0 && \
				unzip -q 'AAAdfIqtIhWjOdOKPMG76RL0a?dl=0'                             
				tar xvf XDM/xdm-setup*.tar.xz
				sudo ./install.sh
				cp XDM/XDMUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAAdfIqtIhWjOdOKPMG76RL0a?dl=0' && rm -Rf XDM
				rm -rf install.sh && rm -rf readme.txt && rm -R javasharedresources
                               ;;
                          78)
			       #Install YoutubeDL+Gui 
			       echo "================ Установка YoutubeDL+Gui ================"
			       sudo apt install -y youtubedl-gui aria2
			       wget https://www.dropbox.com/sh/a145hfqvpzn3axh/AAD_snHKMZjq7hwtqEYecKf8a?dl=0 && \
			       unzip -q 'AAD_snHKMZjq7hwtqEYecKf8a?dl=0'
			       cp YoutubeDl+Gui/YoutubeDL+GuiUninstall.sh ~/'Desktop'/Uninstall
                               rm -rf 'AAD_snHKMZjq7hwtqEYecKf8a?dl=0' && rm -Rf YoutubeDl+Gui                                                                                              	
                               ;;
                          79)
			        #Install Yt-Dlp
			        echo "================ Установка Yt-Dlp ================"
                                sudo apt install -y yt-dlp    
				wget https://www.dropbox.com/sh/vxwefpe3kz91rxe/AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0 && \
				unzip -q 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0'
				chmod +x yt-dlp/yt-dlp-update
				sudo cp yt-dlp/yt-dlp-update /usr/local/bin/
				chmod +x yt-dlp/YoutubeDlpUpdate.desktop
				cp yt-dlp/YoutubeDlpUpdate.desktop ~/.local/share/applications/
				sudo cp yt-dlp/YoutubeDlpUpdate.png /usr/share/icons/
				cp yt-dlp/Yt-DlpUninstall.sh ~/'Desktop'/Uninstall    
				rm -rf 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0' && rm -Rf yt-dlp 
                               ;;
                          80)
			        #Install JDownloader2 
				echo "================ Установка JDownloader2 ================"
                                sudo apt install -y default-jdk
                                wget https://www.dropbox.com/sh/wv25tzc9utsmfzd/AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0 && \
				unzip -q 'AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0'	
		                chmod +x Descargas/JD*
				sudo sh Descargas/JD*.sh
                                chmod +x Descargas/'JDownloader 2-0.desktop'
                                cp Descargas/'JDownloader 2-0.desktop' ~/.local/share/applications/
				cp Descargas/JDownloaderUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAcfrdv3iS3Kg_O2rGqAE_Ha?dl=0' && rm -Rf Descargas
                               ;;
                          81)
			        #Install Videomass
				echo "================ Установка Videomass ================"
                                sudo apt install -y videomass ffmpeg youtube-dl yt-dlp	
                                wget https://www.dropbox.com/sh/6dfe6qouezk9vy2/AABkGkuT3L6w3QsdLhbVlRqSa?dl=0 && \
				unzip -q 'AABkGkuT3L6w3QsdLhbVlRqSa?dl=0'
				cp VideomassApp/VideomassUninstall.sh ~/'Desktop'/Uninstall                           			 				
				rm -rf 'AABkGkuT3L6w3QsdLhbVlRqSa?dl=0' && rm -Rf VideomassApp
				wget https://www.dropbox.com/sh/vxwefpe3kz91rxe/AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0 && \
				unzip -q 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0'
				chmod +x yt-dlp/yt-dlp-update
				sudo cp yt-dlp/yt-dlp-update /usr/local/bin/
				chmod +x yt-dlp/YoutubeDlpUpdate.desktop
				cp yt-dlp/YoutubeDlpUpdate.desktop ~/.local/share/applications/
				sudo cp yt-dlp/YoutubeDlpUpdate.png /usr/share/icons/
				cp yt-dlp/Yt-DlpUninstall.sh ~/'Desktop'/Uninstall    
				rm -rf 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0' && rm -Rf yt-dlp                            
                               ;;
                          82)
			        #Install MediaDownloader
				echo "================  Установка MediaDownloader ================"
                                sudo apt install -y media-downloader yt-dlp
				wget https://www.dropbox.com/sh/2vzwr29ywi8dctq/AABBYlekZpn_qGxB1OR6rVbea?dl=0 && \
				unzip -q 'AABBYlekZpn_qGxB1OR6rVbea?dl=0'
                                tar xvf MediaDownloader/media-downloader.tar.gz 
                                mv media-downloader ~/.config/media-downloader
				cp MediaDownloader/MediaDownloaderUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AABBYlekZpn_qGxB1OR6rVbea?dl=0' && rm -Rf MediaDownloader
				wget https://www.dropbox.com/sh/vxwefpe3kz91rxe/AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0 && \
				unzip -q 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0'
				chmod +x yt-dlp/yt-dlp-update
				sudo cp yt-dlp/yt-dlp-update /usr/local/bin/
				chmod +x yt-dlp/YoutubeDlpUpdate.desktop
				cp yt-dlp/YoutubeDlpUpdate.desktop ~/.local/share/applications/
				sudo cp yt-dlp/YoutubeDlpUpdate.png /usr/share/icons/
				cp yt-dlp/Yt-DlpUninstall.sh ~/'Desktop'/Uninstall    
				rm -rf 'AAAdvdmnkLQc4JLI2hy6W1vQa?dl=0' && rm -Rf yt-dlp
                               ;;
                          83)
			        #Install Motrix
			        echo "================ Motrix ================"
                                sudo apt install -y motrix
				wget https://www.dropbox.com/sh/0jgherokzw8un8t/AABGT5Kp9L9N2di9UFKsxdQza?dl=0 && \
				unzip -q 'AABGT5Kp9L9N2di9UFKsxdQza?dl=0'					
				tar xvf MotrixApp/Motrix.tar.gz    
                                mv Motrix ~/.config/Motrix
				cp MotrixApp/MotrixUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AABGT5Kp9L9N2di9UFKsxdQza?dl=0' && rm -Rf MotrixApp                   		 
                               ;;
                          84)
			        #Install Любая Ваша программа
			        echo "================ Установка Любая Ваша программа ================" 
                               ;;
                          85)
			        #Install Geany
				echo "================ Установка Geany ================"
				wget https://www.dropbox.com/sh/cu7gouwyz2ovbf5/AACkbpyqvUmq9EckFzgPkc_Ka?dl=0 && \
				unzip -q 'AACkbpyqvUmq9EckFzgPkc_Ka?dl=0'
				sudo dpkg -i Geany/geany*.deb
                                sudo apt install -fy
				chmod +x Geany/geany.desktop
                                cp Geany/geany.desktop ~/.local/share/applications/
                                sudo cp Geany/Geany.png /usr/share/icons
				tar xvf Geany/geany.tar.gz
				mv geany ~/.config/geany 
				rm -rf 'AACkbpyqvUmq9EckFzgPkc_Ka?dl=0' && rm -Rf Geany 
                               ;;
                          86)
			        #Install FeatherpadApt
			        echo "================ Установка FeatherpadApt ================"    
                                sudo apt install -y featherpad featherpad-l10n
				wget https://www.dropbox.com/sh/h95quyaq50u9337/AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0 && \
				unzip -q 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0'
				tar xvf  Pluma/featherpad.tar.gz
				mv featherpad ~/.config/featherpad			
				cp Pluma/FeatherPadUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0' && rm -Rf Pluma  
                               ;;
                          87)
			        #Install Pluma
				echo "================ Установка Pluma ================"
                                sudo apt install -y pluma
				wget https://www.dropbox.com/sh/h95quyaq50u9337/AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0 && \
				unzip -q 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0'
				tar xvf  Pluma/pluma.tar.gz
				mv pluma ~/.config/pluma		
				cp Pluma/PlumaUninstall.sh ~/'Desktop'/Uninstall	
				rm -rf 'AABLLDhjc_XNOH8ZzXF_vwQTa?dl=0' && rm -Rf Pluma                          
                               ;;
                          88)
			        #Install Gedit
				echo "================ Установка Gedit ================"
				sudo apt install -y gedit
                                wget https://www.dropbox.com/sh/ye4pn6aattmdt0s/AAAM6euX_TeGh3yOP7hbXpsea?dl=0 && \
				unzip -q 'AAAM6euX_TeGh3yOP7hbXpsea?dl=0'
                                chmod +x Gedit/org.gnome.gedit.desktop
                                cp Gedit/org.gnome.gedit.desktop ~/.local/share/applications/  
                                sudo cp Gedit/Gedit.svg /usr/share/icons/    
				tar xvf Gedit/gedit.tar.gz
				mv gedit ~/.config/
				cp Gedit/GeditUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAM6euX_TeGh3yOP7hbXpsea?dl=0' && rm -Rf Gedit
                               ;;
                          89)
			        #Install MeditApt
				echo "================ Установка MeditApt ================"
				echo 'deb http://download.opensuse.org/repositories/home:/antonbatenev:/medit/Debian_12/ /' | \
				sudo tee /etc/apt/sources.list.d/home:antonbatenev:medit.list
				curl -fsSL https://download.opensuse.org/repositories/home:antonbatenev:medit/Debian_12/Release.key | \
				gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_antonbatenev_medit.gpg > /dev/null
				sudo apt update && sudo apt install -y medit
				wget https://www.dropbox.com/sh/w626a29qh6h9aq6/AACp42Vt8lRAMR72MDwxq63ta?dl=0 && \
				unzip -q 'AACp42Vt8lRAMR72MDwxq63ta?dl=0'								
				tar xvf Medit/medit.tar.gz
                                mv medit ~/.local/share/medit
                                cp Medit/MeditUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AACp42Vt8lRAMR72MDwxq63ta?dl=0' && rm -Rf Medit                                                                  
                               ;;
                          90)
			        #Install Leafpad
		                echo "================ Установка Leafpad ================"
                                wget https://gitlab.com/Chebur70/Setting/-/raw/main/leafpad_0.8.18.1-5_amd64.deb
				sudo dpkg -i leafpad*.deb
				sudo apt install -f -y
				rm -rf leafpad*.deb  
                                wget https://www.dropbox.com/sh/ublhpdts1wlskau/AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0 && \
				unzip -q 'AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0'
                                tar xvf Leafpad/leafpad.tar.gz
                                mv leafpad ~/.config/ 
                                sudo cp Leafpad/Leafpad.png /usr/share/icons/
				cp Leafpad/LeafpadUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAD6ELxDOj4LVGtsFCg4Y0PDa?dl=0' && rm -Rf Leafpad                                                   
                               ;;
                          91)
		                #Install LibreofficeAppImage 
			        echo "================ Установка LibreofficeAppImage ================"
                                wget https://www.dropbox.com/sh/5b4vlk0phzsvi3f/AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0 && \
				unzip -q 'AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0' 	
				chmod +x ./LibreOffice/*.AppImage
				sudo mv LibreOffice/LibreOffice*.AppImage /opt/LibreOffice.AppImage
				chmod +x LibreOffice/*.desktop		                   
				cp LibreOffice/LibreOfficeApp.desktop ~/.local/share/applications/  					
				chmod -R 777 LibreOffice/LibreOffice	
				sudo cp -R LibreOffice/LibreOffice /usr/share/icons/
				cp LibreOffice/LibreofficeUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AAA1kT9j_a1_tOSTp4jDPR9ta?dl=0' && rm -Rf LibreOffice                             		 
                               ;;
                          92)
			        #Install OpenOffice
			        echo "================ Установка OpenOffice ================"	
				sudo apt install -y openoffice openoffice-base openoffice-core* openoffice-images \
				openoffice-ure openoffice-brand* openoffice-calc openoffice-pyuno \
				openoffice-ooofonts openoffice-ooolinguistic openoffice-impress openoffice-math \
				openoffice-writer openoffice-debian-menus
				wget https://www.dropbox.com/sh/inf0p3shz56dggm/AABiwJqm22j3PQg0qRf13pV7a?dl=0 && \
				unzip -q 'AABiwJqm22j3PQg0qRf13pV7a?dl=0'  	
				cp OpenOfficeApp/OpenOfficeUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AABiwJqm22j3PQg0qRf13pV7a?dl=0' && rm -Rf OpenOfficeApp                             
                               ;;
                          93)
			        #Install FreeOffice
		                echo "================ Установка FreeOfficeAppImage ================"
				sudo apt install -y softmaker-freeoffice-2021
				wget https://www.dropbox.com/sh/pfwx54yeguqufpn/AABnFK9WNOUWlu_JEMwpC_Eca?dl=0 && \
				unzip -q 'AABnFK9WNOUWlu_JEMwpC_Eca?dl=0'
				cp FreeOffice/FreeOfficeUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AABnFK9WNOUWlu_JEMwpC_Eca?dl=0' && rm -Rf FreeOffice                                			 	                              
                               ;;
                          94)
			        #Install OnlyOfficeApt
				echo "================ Установка OnlyOfficeApt ================"
				sudo apt install -y onlyoffice-desktopeditors   
                                wget https://www.dropbox.com/sh/ptl3svv61irikb2/AADaNs8m9QVSPup3vrmbMfeVa?dl=0 && 
				unzip -q 'AADaNs8m9QVSPup3vrmbMfeVa?dl=0'
				cp OnlyOffice/OnlyOfficeUninstall.sh ~/'Desktop'/Uninstall
				rm -rf 'AADaNs8m9QVSPup3vrmbMfeVa?dl=0'  && rm -Rf OnlyOffice                                                                              
                               ;;
                          95)
		               #Install WPSOffice
		                echo "================ Установка WPSOfficeAppImage ================"
				sudo apt install -y wps-office
			        wget https://www.dropbox.com/sh/8uf25m1pti1ud3o/AACxBdMG6yX8F6nkchp4zfa-a?dl=0 && 
				unzip -q 'AACxBdMG6yX8F6nkchp4zfa-a?dl=0'
                                cp WPSOffice/WPSOfficeUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AACxBdMG6yX8F6nkchp4zfa-a?dl=0' && rm -Rf WPSOffice                        
                               ;;
                          96)
			        #Install ConkyAll 
			        echo "================ Установка ConkyAll ================"
                                sudo apt install -y conky-all
				wget https://www.dropbox.com/sh/fp9f9ek4cuecpfe/AABNfOcmlk0YnezVWxTjTN2Sa?dl=0 && \
				unzip -q 'AABNfOcmlk0YnezVWxTjTN2Sa?dl=0'
				tar xvf Conky/GreenDark.tar.gz
				sudo mv auzia-conky /opt/auzia-conky
				tar xvf Conky/scripts.tar.gz
				sudo mv scripts /opt/scripts
				tar xvf Conky/start_conky.desktop.tar.gz
				chmod +x start_conky.desktop
				sudo mv start_conky.desktop /etc/xdg/autostart/				
				chmod +x Conky/conky.desktop
                                sudo cp Conky/conky.desktop /etc/xdg/autostart/		                                 
				tar xvf Conky/GrannySmithApple.tar.gz
				sudo mv conky /etc/conky
				cp Conky/ConkyUninstall.sh ~/'Desktop'/Uninstall		
                                rm -rf 'AABNfOcmlk0YnezVWxTjTN2Sa?dl=0' && rm -Rf Conky
                               ;;
                          97)
			        #Install GnomePie
			        echo "================ Установка GnomePie ================"
			        sudo apt install -y gnome-pie
			        wget https://www.dropbox.com/sh/wtctfwmzlxoe9u7/AADDOkDbMEn-Cm96_e9w3wBGa?dl=0 && \
				unzip -q 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0'			        
				tar xvf GnomePie/gnome-pie.tar.gz
				mv gnome-pie ~/.config/gnome-pie
				cp GnomePie/GnomePieUninstall.sh ~/'Desktop'/Uninstall
			        rm -rf 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0' && rm -Rf GnomePie 	                                                       
                               ;;
                          98)
			        #Install Plank
			        echo "================ Установка Plank ================"
			        sudo apt install -y plank
				wget https://www.dropbox.com/sh/wtctfwmzlxoe9u7/AADDOkDbMEn-Cm96_e9w3wBGa?dl=0 && \
				unzip -q 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0'			        
				tar xvf GnomePie/plank.tar.gz
				mv plank ~/.config/plank
				cp GnomePie/plank.desktop ~/.config/autostart/
				cp GnomePie/PlankUninstall.sh ~/'Desktop'/Uninstall
			        rm -rf 'AADDOkDbMEn-Cm96_e9w3wBGa?dl=0' && rm -Rf GnomePie                                                                
                               ;;
                          99)				 
			        #Install Cairo-dock
			        echo "================ Установка Cairo-dock ================"
                                sudo apt install -y cairo-dock
				wget https://www.dropbox.com/sh/so0lexf3aft7diz/AAAJnuUJJa7cUQUIUk2flPwma?dl=0 && \
				unzip -q 'AAAJnuUJJa7cUQUIUk2flPwma?dl=0'
                                chmod +x CairoDock/cairo-dock.desktop
                                cp CairoDock/cairo-dock.desktop ~/.config/autostart/                                 
				tar xvf CairoDock/Bcairo-dock.tar.gz
                                mv cairo-dock ~/.config/cairo-dock
				cp CairoDock/Cairo-dockUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAJnuUJJa7cUQUIUk2flPwma?dl=0' && rm -Rf CairoDock		                              
                               ;;
                        100)
			        #Install РечевойСерверSpeechDispatcher
				echo "================ Установка РечевойСерверSpeechDispatcher ================"
				sudo apt install -y speech-dispatcher speech-dispatcher-audio-plugins speech-dispatcher-espeak-ng \
				espeak espeak-data espeak-ng-data espeakup libespeak-ng1 libespeak1 orca                                
				wget https://www.dropbox.com/sh/61eox35lnu7m7es/AAAG_jvUgmCiIEe9LlnWFblPa?dl=0 && \
				unzip -q 'AAAG_jvUgmCiIEe9LlnWFblPa?dl=0'
                                chmod -R 777 Orca
                                cp Orca/espeak.desktop ~/.local/share/applications/
                                cp Orca/orca-autostart.desktop ~/.config/autostart/ 
                                sudo cp Orca/Espeak.png /usr/share/icons/
                                sudo cp Orca/orca.png /usr/share/icons/
				cp Orca/SpeechDispatcherUninstall.sh ~/'Desktop'/Uninstall
                                rm -rf 'AAAG_jvUgmCiIEe9LlnWFblPa?dl=0' && rm -Rf Orca                             				                              
                               ;;
                        101)
			        #Install Из каталога deb*
		                echo "================ Установка Из каталога deb ======================"
		                cd deb/
		                sudo dpkg -i *.deb
		                sudo apt install -f -y
		                cd ../


	    esac
	done

echo "Запуск обновления плагинов"
sleep 1s
sudo apt install -y xfce4-goodies xfce4-battery-plugin xfce4-clipman xfce4-clipman-plugin \
xfce4-cpufreq-plugin xfce4-datetime-plugin xfce4-diskperf-plugin xfce4-fsguard-plugin \
xfce4-genmon-plugin xfce4-mount-plugin xfce4-xkb-plugin xfce4-sensors-plugin \
xfce4-smartbookmark-plugin xfce4-timer-plugin xfce4-wavelan-plugin \
xfce4-power-manager-plugins xfce4-pulseaudio-plugin
#Установка программ из репозиториев.
echo "Начало установки программ из репозиториев."
sleep 1s
sudo apt install -y gparted menulibre uget vlc vlc-l10n git grub-customizer \
gnome-disk-utility synaptic                                                       
#Доп утилиты
#sudo apt install -y gnome-system-tools network-manager-gnome wireless-tools qt5ct qt5-gtk-platformtheme qt5-style-plugins
echo 'Установка дополнительных программ по мере изучения Linux'
sudo apt install -y tumbler mtpaint thunar-volman hardinfo lshw lshw-gtk \
seahorse psensor gtkhash thunar-gtkhash celluloid mpv winff preload \
neofetch dbus-x11 partitionmanager htop libfuse2
#Игры
#sudo apt install -y aisleriot foobillardplus kpat
#Pulseaudio Alsa
echo 'Устанавливаем звук от Alsa'   #https://alsa-project.org/wiki/Main_Page
sudo apt install pulseaudio gstreamer1.0-pulseaudio pulseaudio-utils xfce4-pulseaudio-plugin \
pavucontrol libpulse-mainloop-glib0 libpulse0 libpulsedsp -y
sudo apt install alsa-utils gstreamer1.0-alsa libasound2 libasound2-data libasound2-plugins -y
#Установка заголовочных файлов ядра   http://rus-linux.net/MyLDP/kernel/kernel-headers.html
sudo apt install -y linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,')
#Установка настроек внешнего вида
#OptSh
wget https://gitlab.com/Chebur70/Setting/-/raw/main/OptSh.tar.gz
tar xvf OptSh.tar.gz && rm -rf OptSh.tar.gz
chmod -R 777 opt
sudo cp -R opt / && rm -Rf opt
#OptApl
wget https://gitlab.com/Chebur70/Setting/-/raw/main/OptApl.tar.gz
tar xvf OptApl.tar.gz && rm -rf OptApl.tar.gz
chmod +x OptApl/*.desktop
cp OptApl/РедактированиеSourcesList.desktop ~/.local/share/applications/
cp OptApl/DebInstall.desktop ~/.local/share/applications/
cp OptApl/Release.desktop ~/.local/share/applications/
cp OptApl/TecmintMonitor.desktop ~/.local/share/applications/
cp OptApl/Xkill.desktop ~/.local/share/applications/
cp OptApl/UpgradePrelink.desktop ~/.local/share/applications/
cp OptApl/Drivers.desktop ~/.local/share/applications/
cp OptApl/LocalesTzdata.desktop ~/.local/share/applications/
cp OptApl/Neofetch.desktop ~/.local/share/applications/
cp OptApl/SystemUpgrade.desktop ~/.local/share/applications/
cp OptApl/OptShAplPngUninstall.sh ~/'Desktop'/Uninstall  
rm -Rf OptApl
#OptPng
wget https://gitlab.com/Chebur70/Setting/-/raw/main/OptPng.tar.gz
tar xvf OptPng.tar.gz && rm -rf OptPng.tar.gz
chmod -R 777 OptPng
sudo mv OptPng /usr/share/icons/
#Install Prelink для ускорения запуска программ в Linux			 
wget https://www.dropbox.com/sh/3zaw6kt99g2rstx/AACFXjA4sG9raoxhU8zSxi8Wa?dl=0 && \
unzip -q 'AACFXjA4sG9raoxhU8zSxi8Wa?dl=0'
sudo dpkg -i Prelink/*.deb
sudo apt install -fy
rm -rf 'AACFXjA4sG9raoxhU8zSxi8Wa?dl=0' && rm -Rf Prelink
#Install MousepadAplPng
sudo apt install -y mousepad
wget https://www.dropbox.com/sh/3sp3cqvbs8i6b4n/AAAXqoYoz8YaC_YakjxJaRAWa?dl=0 && \
unzip -q 'AAAXqoYoz8YaC_YakjxJaRAWa?dl=0'
chmod +x Mousepad/org.xfce.mousepad.desktop
cp Mousepad/org.xfce.mousepad.desktop ~/.local/share/applications/
sudo cp Mousepad/Mousepad.png /usr/share/icons/
cp Mousepad/MousepadUninstall.sh ~/'Desktop'/Uninstall  
rm -rf 'AAAXqoYoz8YaC_YakjxJaRAWa?dl=0' && rm -Rf Mousepad
#Install FirefoxESR
sudo apt install -y firefox-esr firefox-esr-l10n-ru
wget https://www.dropbox.com/sh/5fffc86mpq4dxua/AABEUmmp8wULRoKP2yanxTRka?dl=0 && \
unzip -q 'AABEUmmp8wULRoKP2yanxTRka?dl=0'
chmod +x Firefox/firefox-esr.desktop
cp Firefox/firefox-esr.desktop ~/.local/share/applications/
sudo cp Firefox/Firefox.svg /usr/share/icons/
tar xvf Firefox/mozilla.tar.gz
cp Firefox/FirefoxUninstall.sh ~/'Desktop'/Uninstall
rm -rf 'AABEUmmp8wULRoKP2yanxTRka?dl=0' && rm -Rf Firefox
#Install KlichalexWeb
wget https://www.dropbox.com/sh/wwmeaedbrimnbsl/AAAKupA10I71b5HilCdXsspIa?dl=0 && \
unzip -q 'AAAKupA10I71b5HilCdXsspIa?dl=0'
chmod +x Klichalex/KlichalexF.desktop
cp Klichalex/KlichalexF.desktop ~/'Desktop'/
cp Klichalex/KlichalexF.desktop ~/.local/share/applications/
sudo cp Klichalex/Klichalex.jpg /usr/share/icons/
cp Klichalex/KlichalexWebUninstall.sh ~/'Desktop'/Uninstall
rm -rf 'AAAKupA10I71b5HilCdXsspIa?dl=0' && rm -Rf Klichalex
#ПрозрачныйФонIcons
wget https://gitlab.com/Chebur70/Setting/-/raw/main/gtkrc-2.0.tar.gz
tar xvf gtkrc-2.0.tar.gz && rm -rf gtkrc-2.0.tar.gz
#Рабочий стол и дополнение
wget https://gitlab.com/Chebur70/Setting/-/raw/main/Desktop.tar.gz
tar xvf Desktop.tar.gz && rm -rf Desktop.tar.gz
cp РабочийСтол/Компьютер.desktop ~/'Desktop'/
cp РабочийСтол/КтоИспользуетDebian.sh ~/'Desktop'/
cp РабочийСтол/ПаролиLiveссылки.ctb.pdf ~/'Desktop'/
rm -Rf РабочийСтол
#Install SparkyLiveUsbCreatorPersistence
wget https://www.dropbox.com/sh/a97b8qs6avxkw2w/AAA3tpNMqlfHoIN4nROIqofTa?dl=0 && \
unzip -q 'AAA3tpNMqlfHoIN4nROIqofTa?dl=0'			                                                                             
chmod +x LiveUsb/*.desktop  
cp LiveUsb/sparky-live-usb-creator.desktop ~/.local/share/applications/  
cp LiveUsb/sparky-live-usb-creator-persistence.desktop ~/.local/share/applications/
cp LiveUsb/sparky-usb-formatter.desktop ~/.local/share/applications/  
rm -rf 'AAA3tpNMqlfHoIN4nROIqofTa?dl=0' && rm -Rf LiveUsb
#Внешний Вид
cp -r appearance/xfce4 ~/.config/
cp -r appearance/panel ~/.config/
#прописываем владельца каталога xfce4
chown -R ${name}:${name} ~/.config/xfce4/
chown -R ${name}:${name} ~/.config/panel/
#удаление папки appearance
rm -Rf appearance

echo "Давайте настроим автоматический запуск системы."
echo "(Если Вы, конечно, это хотите)"
echo "===================================="
echo ""
echo ""
echo "Автоматический вход в Xfce настраивается путем редактирования файла «/etc/lightdm/lightdm.conf»."
echo "Для этого необходимо найти в пункте [Seat:*] две строки :"
echo "#autologin-user="
echo "#autologin-user-timeout=0"
echo "И изменить их, раскоментировать и прописать свой логин, например:"
echo "autologin-user=alex  #вместо alex Ваш логин"
echo "autologin-user-timeout=0"
echo "Сохранить изменения конфига"
echo "Запустить lightdm.conf для редактирования?"
echo "y - запустить, любой другой символ - нет"
read doing 
case $doing in
y)
 sudo mousepad /etc/lightdm/lightdm.conf
 ;;
*)
 echo 'Редактирование lightdm.conf отменено'
esac #окончание оператора case.
echo ""
echo ""

echo 'Операция настройки системы завершена'
echo ""
echo ""
echo ""
echo "Хотите настроить автоматический запуск системы?"
echo "Автоматический вход в Xfce настраивается путем редактирования файла «/etc/sddm.conf»."
echo "#Было"
echo "[Autologin]"
echo "#User=Ваш USERNAME"
echo "#Session=xfce.desktop"
echo "И изменить их, раскоментировать и прописать свой логин, например:"
echo "[Autologin]"
echo "User=Ваш USERNAME"
echo "Session=xfce.desktop"
echo "timeout=5"
echo "Запустить sddm.conf для редактирования?"
echo "y - запустить, любой другой символ - нет"
read doing 
case $doing in
y)
  sudo mousepad /etc/sddm.conf
 ;;
*)
 echo 'Редактирование sddm.conf отменено'
esac #окончание оператора case.
echo ""
echo ""
echo ""

#Удаление лишних файлов и папок
sudo rm -rf /etc/xdg/autostart/anydesk_global_tray.desktop 
sudo rm -rf /etc/xdg/autostart/polkit-gnome-authentication-agent-1.desktop
rm -Rf appearance
rm -Rf ~/.config/openbox
rm -Rf ~/.config/pcmanfm
echo 'Обновление и очистка после всех установок'
sudo apt full-upgrade -y
sudo apt autoremove -y
#очищает папку /var/cache/apt/archives;
sudo aptitude clean -y
#Очистка APT кеш 
sudo apt clean -y

echo "Скрипт работу закончил."
echo "Спасибо klichalex автору скрипта Transformation! https://www.youtube.com/user/klichalex/featured"
echo "Спасибо klichalex автору хороших видео уроков! https://rutube.ru/channel/23628980/videos/"
echo "Спасибо klichalex автору перевода Refracta на Русский язык http://prostolinux.my1.ru/"
echo "Спасибо circulosmeos https://github.com/circulosmeos за предоставленые скрипты"
echo "Спасибо разработчику скриптов Dmitriy wj42ftns Chekhov, Russia https://gist.github.com/wj42ftns"
echo "Спасибо kachnu https://github.com/kachnu за предоставленые скрипты"
echo "Спасибо Systemback https://gitlab.com/Kendek/systemback"
echo "Спасибо LeCorbeau's Vault https://lecorbeausvault.wordpress.com/2021/01/10/quickly-build-a-custom-bootable-installable-debian-live-iso-with-live-build/"
echo "Спасибо Франко Кониди https://syslinuxos.com/things-to-do-after-installing-syslinuxos-12/"
echo "Нет недостижимых целей,есть высокий коэффициент лени,недостаток смекалкии и запас отговорок"
echo "Не важно насколько медленно ты движешься, главное не останавливаться.Конфуций"
echo "Наш ответ Чемберлену!Дави Империализма Гиену Могучий Рабочий Класс!" 
echo "Вчера были танки лишь у Чемберлена,А нынче есть и у нас!"
echo "Запомните,чтобы ничего не делать, надо уметь делать все" 
echo "Я не потерпел неудачу, я нашел 10 000 способов, которые не сработают-Эдисон."
echo "Операция настройки системы завершена"
echo "Выйти из настроек, или перезапустить систему?"
echo "y - выйти, любой другой символ - перезапуск"
read doing 
case $doing in
y)
  exit
 ;;
*)
sudo reboot -f
esac #окончание оператора case.
